from setuptools import setup

setup(
    name="odoo-la-borda",
    version="0.0.1",
    author="Coopdevs",
    description="Odoo customizations for La Borda",
    license="GPL",
    packages=['la-borda'],
    classifiers=[],
)
